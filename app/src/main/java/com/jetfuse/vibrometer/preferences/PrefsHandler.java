package com.jetfuse.vibrometer.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.jetfuse.vibrometer.alarm.VibrometerAlarm;

import java.util.StringTokenizer;

public class PrefsHandler {
    public static final String APP_PREFS = "jetfuse.vibrometer";

    public static final String KEY_ALARM_INTERVAL = "interval";
    public static final String KEY_PLAY_NOTIFICATION_SOUND = "playNotificationSound";
    public static final String KEY_ALARM_NOTIFICATION_SOUND = "notificationSound";
    public static final String KEY_ALARM_VIBRATION_PATTERN_LENGTH = "alarmVibrationPatternLength";
    public static final String KEY_ALARM_VIBRATION_PATTERN = "alarmVibrationPattern";

    public static final String KEY_ALARM_ENABLED = "enabled";

    private static PrefsHandler prefsHandler;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;

    public static PrefsHandler instantiatePrefsHandler(Context context) {
        if (prefsHandler == null) {
            prefsHandler = new PrefsHandler(context);
        }

        return prefsHandler;
    }

    public static PrefsHandler getPrefsHandler() {
        //May be null, in special cases where an activity may not have
        //instantiated a prefs handler (eg. widgets, intent launches),
        //instantiatePrefsHandler should be used, if possible.
        return prefsHandler;
    }

    private PrefsHandler(Context context) {
        /**
         * Creates an PrefsHandler object that is responsible for reading and writing shared preferences.
         *
         * @param context  an application context to get shared preferences for
         */

        sharedPreferences = context.getSharedPreferences(APP_PREFS, Activity.MODE_PRIVATE);

        sharedPreferencesEditor = sharedPreferences.edit();
    }

    public void saveAlarm(VibrometerAlarm alarm) {
        final String INTERVAL = buildAlarmKey(KEY_ALARM_INTERVAL, alarm.getId());
        final String PLAY_NOTIFICATION = buildAlarmKey(KEY_PLAY_NOTIFICATION_SOUND, alarm.getId());
        final String NOTIFICATION = buildAlarmKey(KEY_ALARM_NOTIFICATION_SOUND, alarm.getId());
        final String VIBRATION_PATTERN_LENGTH = buildAlarmKey(KEY_ALARM_VIBRATION_PATTERN_LENGTH, alarm.getId());
        final String VIBRATION_PATTERN = buildAlarmKey(KEY_ALARM_VIBRATION_PATTERN, alarm.getId());

        sharedPreferencesEditor.putInt(INTERVAL, alarm.getInterval());
        sharedPreferencesEditor.putBoolean(PLAY_NOTIFICATION, alarm.playNotificationSound());

        if (alarm.getNotificationSound() != null) {
            sharedPreferencesEditor.putString(NOTIFICATION, alarm.getNotificationSound().toString());
        }
        else {
            sharedPreferencesEditor.putString(NOTIFICATION, null);
        }

        sharedPreferencesEditor.putInt(VIBRATION_PATTERN_LENGTH, alarm.getVibrationPattern().length);
        sharedPreferencesEditor.putString(VIBRATION_PATTERN, patternToString(alarm.getVibrationPattern()));

        sharedPreferencesEditor.commit();
    }

    public VibrometerAlarm getAlarm(int id) {
        final String INTERVAL = buildAlarmKey(KEY_ALARM_INTERVAL, id);
        final String PLAY_NOTIFICATION = buildAlarmKey(KEY_PLAY_NOTIFICATION_SOUND, id);
        final String NOTIFICATION = buildAlarmKey(KEY_ALARM_NOTIFICATION_SOUND, id);
        final String VIBRATION_PATTERN_LENGTH = buildAlarmKey(KEY_ALARM_VIBRATION_PATTERN_LENGTH, id);
        final String VIBRATION_PATTERN = buildAlarmKey(KEY_ALARM_VIBRATION_PATTERN, id);

        int interval = sharedPreferences.getInt(INTERVAL, -1);
        boolean playNotification = sharedPreferences.getBoolean(PLAY_NOTIFICATION, false);
        String uriString = sharedPreferences.getString(NOTIFICATION, null);
        int patternLength = sharedPreferences.getInt(VIBRATION_PATTERN_LENGTH, -1);
        String patternString = sharedPreferences.getString(VIBRATION_PATTERN, null);

        if ((interval == -1) || (patternLength == -1) || (patternString == null)) {
            return null;
        }

        long[] vibrationPattern = stringToPattern(patternString, patternLength);

        if (uriString != null) {
            return new VibrometerAlarm(id, interval, playNotification, Uri.parse(uriString), vibrationPattern);
        }
        else {
            return new VibrometerAlarm(id, interval, playNotification, null, vibrationPattern);
        }
    }

    public void enableAlarm(int id) {
        final String ENABLED = buildAlarmKey(KEY_ALARM_ENABLED, id);

        sharedPreferencesEditor.putBoolean(ENABLED, true);

        sharedPreferencesEditor.commit();
    }

    public void disableAlarm(int id) {
        final String ENABLED = buildAlarmKey(KEY_ALARM_ENABLED, id);

        sharedPreferencesEditor.putBoolean(ENABLED, false);

        sharedPreferencesEditor.commit();
    }

    public boolean alarmEnabled(int id) {
        final String ENABLED = buildAlarmKey(KEY_ALARM_ENABLED, id);

        return sharedPreferences.getBoolean(ENABLED, false);
    }

    private static String buildAlarmKey(String keyPrefix, int alarmId) {
        return keyPrefix + "-" + alarmId;
    }

    private static String patternToString(long[] vibrationPattern) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < vibrationPattern.length; i++) {
            stringBuilder.append(vibrationPattern[i]).append(",");
        }

        return stringBuilder.toString();
    }

    private static long[] stringToPattern(String vibrationPatternString, int length) {
        StringTokenizer tokenizer = new StringTokenizer(vibrationPatternString, ",");

        long[] vibrationPattern = new long[length];

        for (int i = 0; i < length; i++) {
            vibrationPattern[i] = Long.parseLong(tokenizer.nextToken());
        }

        return vibrationPattern;
    }
}
