package com.jetfuse.vibrometer.alarm;

import android.net.Uri;

public class VibrometerAlarm {
    private int id;
    private int interval;
    private boolean playNotificationSound;
    private Uri notificationSound;
    private long[] vibrationPattern;

    public VibrometerAlarm(int id, int interval, boolean playNotificationSound, Uri notificationSound, long[] vibrationPattern) {
        this.id = id;
        this.interval = interval;
        this.playNotificationSound = playNotificationSound;
        this.notificationSound = notificationSound;
        this.vibrationPattern = vibrationPattern;
    }

    public int getId() {
        return id;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int newInterval) {
        interval = newInterval;
    }

    public boolean playNotificationSound() {
        return playNotificationSound;
    }

    public void playNotificationSound(boolean playNotificationSound) {
        this.playNotificationSound = playNotificationSound;
    }

    public Uri getNotificationSound() {
        return notificationSound;
    }

    public void setNotificationSound(Uri newNotificationSound) {
        notificationSound = newNotificationSound;
    }

    public long[] getVibrationPattern() {
        return vibrationPattern;
    }

    public void setVibrationPattern(long[] newVibrationPattern) {
        vibrationPattern = newVibrationPattern;
    }
}
