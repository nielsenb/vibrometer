package com.jetfuse.vibrometer.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jetfuse.vibrometer.activities.MainActivity;
import com.jetfuse.vibrometer.preferences.PrefsHandler;

public class RebootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            PrefsHandler prefsHandler = PrefsHandler.instantiatePrefsHandler(context);

            VibrometerAlarm alarm = prefsHandler.getAlarm(MainActivity.ALARM_ID);

            if (alarm != null && prefsHandler.alarmEnabled(alarm.getId()) == true) {
                Scheduler.setAlarm(context, alarm);
            }
        }
    }
}
