package com.jetfuse.vibrometer.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class Scheduler {
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int SECONDS_PER_MINUTE = 60;

    public static void setAlarm(Context context, VibrometerAlarm alarm) {
        long alarmTime = System.currentTimeMillis() + alarm.getInterval() * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(AlarmReceiver.INTENT_VIBROMETER_ALARM);
        intent.putExtra(AlarmReceiver.EXTRA_ALARM_ID, alarm.getId());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarm.getId(), intent, 0);

        //Schedule alarm to happen at interval minutes from now
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);
        }
        else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);
        }
    }

    public static void cancelAlarm(Context context, int id) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(AlarmReceiver.INTENT_VIBROMETER_ALARM);
        PendingIntent sender = PendingIntent.getBroadcast(context, id, intent, 0);

        alarmManager.cancel(sender);
    }
}
