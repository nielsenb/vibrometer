package com.jetfuse.vibrometer.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.jetfuse.vibrometer.preferences.PrefsHandler;
import com.jetfuse.vibrometer.vibration.Pattern;

public class AlarmReceiver extends BroadcastReceiver {
    public static final String INTENT_VIBROMETER_ALARM = "jetfuse.vibrometer.START_ALARM"; //Must match AndroidManifest.xml

    public static final String EXTRA_ALARM_ID = "jetfuse.vibrometer.ALARM_ID";

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

        wakeLock.acquire();

        //Get the alarm
        PrefsHandler prefsHandler = PrefsHandler.instantiatePrefsHandler(context);

        int alarmId = intent.getIntExtra(EXTRA_ALARM_ID, -1);

        if (alarmId != -1 && prefsHandler.alarmEnabled(alarmId) == true) {
            VibrometerAlarm alarm = prefsHandler.getAlarm(alarmId);

            if (alarm != null ) {
                //Play the tone
                if (alarm.playNotificationSound() == true && alarm.getNotificationSound() != null) {
                    try {
                        Ringtone ringtone = RingtoneManager.getRingtone(context, alarm.getNotificationSound());
                        ringtone.play();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                //Perform the vibration
                Pattern.playPattern(context, alarm.getVibrationPattern());

                //Schedule the next alarm
                Scheduler.setAlarm(context, alarm);
            }
        }

        wakeLock.release();
    }
}
