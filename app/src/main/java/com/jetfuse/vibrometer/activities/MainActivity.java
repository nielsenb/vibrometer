package com.jetfuse.vibrometer.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jetfuse.vibrometer.R;
import com.jetfuse.vibrometer.alarm.Scheduler;
import com.jetfuse.vibrometer.alarm.VibrometerAlarm;
import com.jetfuse.vibrometer.preferences.PrefsHandler;
import com.jetfuse.vibrometer.vibration.Pattern;
import com.jetfuse.vibrometer.vibration.Patterns;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, SeekBar.OnSeekBarChangeListener {
    public static final int REQUEST_SELECT_NOTIFICATION_SOUND = 3;

    public static final int ALARM_ID = 77; //TODO: Support multiple alarms

    private PrefsHandler prefsHandler;

    private VibrometerAlarm vibrometerAlarm;

    private TextView labelInterval;
    private SeekBar seekbarInterval;
    private Switch switchNotificationSound;
    private Button btnSelectNotificationSound;
    private Button btnStartStopAlarm;

    //Used by the vibration pattern selection dialog
    private Pattern selectedVibrationPattern = Patterns.patterns.get(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Grab some views
        labelInterval = (TextView) findViewById(R.id.labelInterval);
        seekbarInterval = (SeekBar) findViewById(R.id.seekbarInterval);
        switchNotificationSound = (Switch) findViewById(R.id.switchPlayNotificationSound);
        btnSelectNotificationSound = (Button) findViewById(R.id.btnSelectNotificationSound);
        btnStartStopAlarm = (Button) findViewById(R.id.btnStartStopAlarm);

        prefsHandler = PrefsHandler.instantiatePrefsHandler(this);

        //Set up state
        vibrometerAlarm = prefsHandler.getAlarm(ALARM_ID);

        if (vibrometerAlarm == null) {
            //Create a 'default' alarm
            vibrometerAlarm = new VibrometerAlarm(ALARM_ID, 15, false, null, Patterns.patterns.get(0).getPattern());
        }

        if (vibrometerAlarm.getNotificationSound() == null) {
            switchNotificationSound.setChecked(false);
            btnSelectNotificationSound.setEnabled(false);
        }
        else {
            switchNotificationSound.setChecked(vibrometerAlarm.playNotificationSound());
            btnSelectNotificationSound.setEnabled(true);
        }

        if (prefsHandler.alarmEnabled(ALARM_ID)) {
            btnStartStopAlarm.setText(getString(R.string.stop_alarm));
        }
        else {
            btnStartStopAlarm.setText(getString(R.string.start_alarm));
        }

        labelInterval.setText(Integer.toString(vibrometerAlarm.getInterval()));

        seekbarInterval.setProgress(intervalToProgress(vibrometerAlarm.getInterval()));
        seekbarInterval.setMax(285); //15 minutes is added to account for the fact the minimum value is 0, but the 'real' minimum is 15
        seekbarInterval.setOnSeekBarChangeListener(this);

        //Set up the switch listener
        switchNotificationSound.setOnCheckedChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        labelInterval.setText(Integer.toString(progressToInterval(progress)));

        //Update the interval
        vibrometerAlarm.setInterval(progressToInterval(seekbarInterval.getProgress()));
        prefsHandler.saveAlarm(vibrometerAlarm);

        //Since we changed the interval, reset the alarm
        Scheduler.cancelAlarm(this, ALARM_ID);
        Scheduler.setAlarm(this, vibrometerAlarm);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (switchNotificationSound.isChecked()) {
            vibrometerAlarm.playNotificationSound(true);
            btnSelectNotificationSound.setEnabled(true);
        }
        else {
            vibrometerAlarm.playNotificationSound(false);
            btnSelectNotificationSound.setEnabled(false);
        }

        //Save the pref
        prefsHandler.saveAlarm(vibrometerAlarm);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
    {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_NOTIFICATION_SOUND) {
            Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (uri != null) {
                vibrometerAlarm.setNotificationSound(uri);
                prefsHandler.saveAlarm(vibrometerAlarm);
            }
            else {
                Toast.makeText(this, getText(R.string.no_notification_sound_selected), Toast.LENGTH_LONG).show();
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED && requestCode == REQUEST_SELECT_NOTIFICATION_SOUND) {
            Toast.makeText(this, getText(R.string.no_notification_sound_selected), Toast.LENGTH_LONG).show();
        }
    }

    public void selectVibrationPattern(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.select_vibration_pattern)
                .setSingleChoiceItems(Patterns.getNames(), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // If the user checked the item, add it to the selected items
                        selectedVibrationPattern = Patterns.getByName(Patterns.getNames()[which]);
                        Pattern.playPattern(getApplicationContext(), selectedVibrationPattern.getPattern());
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Save the preference
                        vibrometerAlarm.setVibrationPattern(selectedVibrationPattern.getPattern());
                        prefsHandler.saveAlarm(vibrometerAlarm);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getApplicationContext(), getText(R.string.no_vibration_pattern_selected), Toast.LENGTH_LONG).show();
                    }
                });

        builder.show();
    }

    public void selectNotificationTone(View view) {
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);

        startActivityForResult(intent, REQUEST_SELECT_NOTIFICATION_SOUND);
    }

    public void toggleAlarm(View view) {
        if (prefsHandler.alarmEnabled(ALARM_ID) == true) {
            stopAlarm();
        }
        else {
            startAlarm();
        }
    }

    public void startAlarm() {
        btnStartStopAlarm.setText(getString(R.string.stop_alarm));

        Scheduler.setAlarm(this, vibrometerAlarm);

        prefsHandler.enableAlarm(ALARM_ID);
    }

    public void stopAlarm() {
        btnStartStopAlarm.setText(getString(R.string.start_alarm));

        Scheduler.cancelAlarm(this, ALARM_ID);

        prefsHandler.disableAlarm(ALARM_ID);
    }

    private static int progressToInterval(int progress) {
        //Given a progress value from 0 to 285, make an interval from 15 to 300 (minutes)
        return progress + 15;
    }

    private static int intervalToProgress(int interval) {
        //Given an interval from 15 to 300, make a progress value from 0 to 285
        return interval - 15;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //Don't care
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //Don't care
    }
}
