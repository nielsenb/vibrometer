package com.jetfuse.vibrometer.vibration;

import java.util.ArrayList;
import java.util.List;

public class Patterns {
    public static final List<Pattern> patterns = new ArrayList<Pattern>() {{
        add(new Pattern("Zoom Zoom", new long[] {Pattern.NO_PAUSE, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE, Pattern.SHORT_BUZZ}));
        add(new Pattern("Awooga Awooga", new long[] {Pattern.NO_PAUSE, Pattern.LONG_BUZZ, Pattern.SHORT_PAUSE, Pattern.LONG_BUZZ}));
        add(new Pattern("Kazoo", new long[] {Pattern.NO_PAUSE, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE, Pattern.MEDIUM_BUZZ, Pattern.SHORT_PAUSE, Pattern.LONG_BUZZ}));
        add(new Pattern("Doot Doot", new long[] {Pattern.NO_PAUSE, Pattern.MEDIUM_BUZZ, Pattern.SHORT_PAUSE, Pattern.MEDIUM_BUZZ}));;
        add(new Pattern("Shave and a Haircut", new long[] {Pattern.NO_PAUSE, Pattern.MEDIUM_BUZZ, Pattern.SHORT_PAUSE, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.MEDIUM_BUZZ, Pattern.SHORT_PAUSE, Pattern.MEDIUM_BUZZ, Pattern.LONG_PAUSE, Pattern.MEDIUM_BUZZ, Pattern.SHORT_PAUSE, Pattern.MEDIUM_BUZZ}));
        add(new Pattern("Davy Jones", new long[] {Pattern.NO_PAUSE, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE, Pattern.LONG_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.LONG_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.LONG_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.SHORT_BUZZ, Pattern.SHORT_PAUSE / 2, Pattern.SHORT_BUZZ}));
    }};

    public static String[] getNames() {
        String[] names = new String[patterns.size()];

        for(int i = 0; i < patterns.size(); i++) {
            names[i] = patterns.get(i).getName();
        }

        return names;
    }

    public static Pattern getByName(String name) {
        for (Pattern pattern : patterns) {
            if (pattern.getName().equals(name)) {
                return pattern;
            }
        }

        return null;
    }
}
