package com.jetfuse.vibrometer.vibration;

import android.content.Context;
import android.os.Vibrator;

public class Pattern {
    private static final long DURATION_NONE = 0;
    private static final long DURATION_SHORT = 250;
    private static final long DURATION_MEDIUM = 500;
    private static final long DURATION_LONG = 1000;

    public static final long NO_BUZZ = DURATION_NONE;
    public static final long SHORT_BUZZ = DURATION_SHORT;
    public static final long MEDIUM_BUZZ = DURATION_MEDIUM;
    public static final long LONG_BUZZ = DURATION_LONG;

    public static final long NO_PAUSE = DURATION_NONE;
    public static final long SHORT_PAUSE = DURATION_SHORT;
    public static final long MEDIUM_PAUSE = DURATION_MEDIUM;
    public static final long LONG_PAUSE = DURATION_LONG;

    private String name;
    private long[] pattern;

    public Pattern(String name, long[] pattern) {
        this.name = name;
        this.pattern = pattern;
    }

    public String getName() {
        return this.name;
    }

    public long[] getPattern() {
        return pattern;
    }

    public static void playPattern(Context context, long[] pattern) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        vibrator.vibrate(pattern, -1);
    }
}
